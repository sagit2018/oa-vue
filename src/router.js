import Vue from 'vue'
import Router from 'vue-router'
const Login = () => import("./views/Login.vue")
// import Login from "@/views/Login.vue"

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: "/login",
      component: Login
    },
    {
      path: "/",
      component: () => import("@/components/Home.vue"),
      redirect: "/welcome",
      children: [
        {
          path: "/welcome",
          component: () => import("@/components/subcomponents/Welcome.vue")
        },
        // {
        //   path: "/users",
        //   component: () => import("@/components/users/Users.vue")
        // },
        // {
        //   path: "/roles",
        //   component: () => import("@/components/right/Roles.vue")
        // },
        // {
        //   path: "/rights",
        //   component: () => import("@/components/right/Right.vue")
        // },
        // {
        //   path: "/goods",
        //   component: () => import("@/components/goods/Goods.vue")
        // },
        // {
        //   path: "/params",
        //   component: () => import("@/components/goods/Params.vue")
        // },
        // {
        //   path: "/categories",
        //   component: () => import("@/components/goods/Categories.vue")
        // },
        // {
        //   path: "/orders",
        //   component: () => import("@/components/orders/Orders.vue")
        // },
        // {
        //   path: "/reports",
        //   component: () => import("@/components/reports/Reports.vue")
        // },
      ]
    }
  ]
})

  router.beforeEach((to, from, next) => {
    // 为路由注册全局导航守卫
    // console.log(to, from, next);
    let token = localStorage.getItem("token")
    if(token){
      // 已登录，则放行
      next()
    } else {
      // 未登录，且即将访问非登录页面，则强制跳转至登录页面
      if(to.path !== "/login") {
        next({path: "/login"})
      } else {
        // 未登录，且即将访问登录页面，则放行
        next()
      }
    }
  })

export default router