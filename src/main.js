import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// 引入公共css文件
import "@/css/index.css"

// 引入element-ui
import ElementUi from "element-ui"
import "element-ui/lib/theme-chalk/index.css"
Vue.use(ElementUi)

new Vue({
  created() {
    console.log("入口文件被读取了");
    // 将动态生成的  二级路由路径数组  追加到全局路由选项中去   括号中的方法是执行vuex中的getters方法，获取左侧菜单数据，返回的是一个新数组，使用扩展运算符将新数组添加到二级路有中
    this.$router.options.routes[1].children.push(...this.$store.getters.getRouter)
    // 手动刷新路由列表
    this.$router.addRoutes(this.$router.options.routes)
  },
  router,
  store,
  render: h => h(App)
}).$mount('#app')
