import Vue from 'vue'
import Vuex from 'vuex'
import { getMenu } from "@/api/index.js"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    menu: JSON.parse(localStorage.getItem("menu")) || []
  },
  mutations: {
    getMenuData(state){
      // 通过api获取所有的左侧菜单信息
      getMenu().then(res => {
        // 获取之后保存到本地的状态属性中
        state.menu = res.data.data
        // 同时在保存到本地
        localStorage.setItem("menu", JSON.stringify(state.menu))
      })
    }
  },
  getters: {
    getRouter(state){
      var menuArr = []
      // 遍历路由路径数组，将其进行包装
      state.menu.forEach(item => {
        item.children.forEach(subitem => {
          var menuObj = {}
          menuObj.path = "/" + subitem.path
          // 动态生成路由规则
          menuObj.component = () => import(`@/components/${item.path}/${subitem.path}.vue`)
          // 将包装好的数据添加到新数组中
          menuArr.push(menuObj)
        })
      })
      // 返回新数组
      return menuArr
    }
  },
  actions: {
    // 发送异步请求
    getMenuAsync(context){
      context.commit("getMenuData")
    }

  }
})
