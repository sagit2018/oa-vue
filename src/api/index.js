import axios from "axios"
axios.defaults.baseURL = "http://www.lovegf.cn:8888/api/private/v1/"

// 设置请求拦截器,为所有的异步请求加上token
axios.interceptors.request.use(config => {
// Do something before request is sent
    // 取出本地存储的token值
    let token = localStorage.getItem("token")
    // 设置请求头中Authorization,以符合后台的规定
    config.headers["Authorization"] = token
    return config;
},error => {
// Do something with request error
    return Promise.reject(error);
});

// 用户登录请求
export function getLogin(params){
    return axios.post("login", params)
}

// 请求用户数据列表
export const getUserList = params => {
    return axios.get("users", params)
}

// 请求侧边栏数据
export const getMenu = () => {
    return axios.get("menus")
}